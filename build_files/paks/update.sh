#!/bin/bash
############################################################################
#                                                                          #
# This file is part of the IPFire Firewall.                                #
#                                                                          #
# IPFire is free software; you can redistribute it and/or modify           #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation; either version 2 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# IPFire is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with IPFire; if not, write to the Free Software                    #
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA #
#                                                                          #
# Copyright (C) 2007 IPFire-Team <info@ipfire.org>.                        #
#                                                                          #
############################################################################
#
. /opt/pakfire/lib/functions.sh

# Configuration file paths
CONFPATH="/var/pgsql/data"
HBACONF="pg_hba.conf"
POSTGRESCONF="postgresql.conf"

## Save configuration files in case they are modified
# Save pg_hba.conf
if [ -e "${CONFPATH}/${HBACONF}"  ]; then
	cp ${CONFPATH}/${HBACONF} /tmp
fi

# Save postgresql conf
if [ -e ${CONFPATH}/${POSTGRESCONF} ]; then
	cp ${CONFPATH}/${POSTGRESCONF} /tmp
fi

echo "Saved old configuration files to /tmp and prepared for update... "
sleep 2

./uninstall.sh

./install.sh

# Save default configuration files as '*.orig' for potential changes/merges
cp ${CONFPATH}/${HBACONF} ${CONFPATH}/${HBACONF}.orig
cp ${CONFPATH}/${POSTGRESCONF} /${CONFPATH}/${POSTGRESCONF}.orig

## Play back old configuration files
# Play back pg_hba.conf
mv /tmp/${HBACONF} ${CONFPATH}
# Play back postgres.conf
mv /tmp/${POSTGRESCONF} ${CONFPATH}

echo "Played old configuration files back and will restart PostgreSQL now... "

# Restart PostgreSQL after configuration changes
/etc/init.d/postgresql restart
sleep 2

# EOF


