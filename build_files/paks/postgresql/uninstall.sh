#!/bin/bash
############################################################################
#                                                                          #
# This file is part of the IPFire Firewall.                                #
#                                                                          #
# IPFire is free software; you can redistribute it and/or modify           #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation; either version 2 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# IPFire is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with IPFire; if not, write to the Free Software                    #
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA #
#                                                                          #
# Copyright (C) 2007 IPFire-Team <info@ipfire.org>.                        #
#                                                                          #
############################################################################
#
. /opt/pakfire/lib/functions.sh

NAME="postgresql"

# Stop processes
/etc/init.d/${NAME} stop

# Check that the instances are really off
if pidof -x "${NAME}" >/dev/null; then
    /etc/init.d/${NAME} stop;
fi

#extract_backup_includes
#make_backup ${NAME}
#remove_files

# Remove Files
rm -rvf \
/etc/rc.d/init.d/postgresql \
/etc/logrotate.d/postgresql \
/opt/pakfire/db/installed/meta-${NAME} \
/usr/bin/clusterdb \
/usr/bin/createdb \
/usr/bin/createuser \
/usr/bin/dropdb \
/usr/bin/dropuser \
/usr/bin/ecpg \
/usr/bin/initdb \
/usr/bin/pg_* \
/usr/bin/pgbench \
/usr/bin/postgres \
/usr/bin/postmaster \
/usr/bin/psql \
/usr/bin/reindexdb \
/usr/bin/vacuumdb \
/usr/lib/libecpg* \
/usr/lib/libpgtypes.so* \
/usr/lib/libpq.so* \
/usr/lib/postgresql \
/usr/share/postgresql \
/var/log/postgresql \
/run/postgresql \
/var/pgsql

# Delete old symlink if presant
rm -rfv /etc/rc.d/rc?.d/???${NAME};

# EOF

