#!/bin/bash
############################################################################
#                                                                          #
# This file is part of the IPFire Firewall.                                #
#                                                                          #
# IPFire is free software; you can redistribute it and/or modify           #
# it under the terms of the GNU General Public License as published by     #
# the Free Software Foundation; either version 2 of the License, or        #
# (at your option) any later version.                                      #
#                                                                          #
# IPFire is distributed in the hope that it will be useful,                #
# but WITHOUT ANY WARRANTY; without even the implied warranty of           #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the            #
# GNU General Public License for more details.                             #
#                                                                          #
# You should have received a copy of the GNU General Public License        #
# along with IPFire; if not, write to the Free Software                    #
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307 USA #
#                                                                          #
# Copyright (C) 2007 IPFire-Team <info@ipfire.org>.                        #
#                                                                          #
############################################################################
#
. /opt/pakfire/lib/functions.sh

NAME="postgresql"

extract_files
#restore_backup ${NAME}

## Add symlinks
# Possible runlevel ranges
echo;
echo "Will set symlinks to activate the initscript for start|stop|reboot";
echo
SO="[6-9][0-9]";
SA="[1-4][0-9]";
RE="[1-4][0-9]";
# Search free runlevel
STOP=$(ls /etc/rc.d/rc0.d/ | sed -e 's/[^0-9]//g' | awk '$1!=p+1{print p+1" "$1-1}{p=$1}' | sed -e '1d' | tr ' ' '\n' | grep -E "${SO}" | head -1);
START=$(ls /etc/rc.d/rc3.d/ | sed -e 's/[^0-9]//g' | awk '$1!=p+1{print p+1" "$1-1}{p=$1}' | sed -e '1d' | tr ' ' '\n' | grep -E "${SA}" | head -1);
REBOOT=$(ls /etc/rc.d/rc6.d/ | sed -e 's/[^0-9]//g' | awk '$1!=p+1{print p+1" "$1-1}{p=$1}' | sed -e '1d' | tr ' ' '\n' | grep -E "${RE}" | head -1);
## Add symlinks 
ln -s ../init.d/${NAME} /etc/rc.d/rc0.d/K${STOP}${NAME};
ln -s ../init.d/${NAME} /etc/rc.d/rc3.d/S${START}${NAME};
ln -s ../init.d/${NAME} /etc/rc.d/rc6.d/K${REBOOT}${NAME};
echo

# Add meta file for IPFire WUi status section
if ! ls /opt/pakfire/db/installed/ | grep -q "meta-${NAME}"; then
  touch /opt/pakfire/db/installed/meta-${NAME};
  echo "Have added now meta file for de- or activation via IPfire WUI ${NAME} on reboot... ";
else
  echo "${NAME} meta file has already been set, will do nothing... ";
fi
echo

# Add user and group for postgresql if not already done
if ! grep -q "postgres" /etc/passwd; then
    groupadd -g 41 postgres;
    useradd -c "PostgreSQL Server" -g postgres -d /var/pgsql/data -s /sbin/nologin postgres;
    echo;
    echo "Have add user and group 'postgres'";
else
    echo;
    echo "User already presant leave it as it is";
fi
echo

# Add working and run directory
install -v -dm700 /var/pgsql/data
install -v -dm755 /run/postgresql
# Set permissions
chown -Rv postgres:postgres /var/pgsql /run/postgresql /var/log/postgresql
echo

# Initialize database
echo "Initializing database"
su -s /bin/bash - postgres -c '/usr/bin/initdb -D /var/pgsql/data'
echo

# Starting postgresql
/etc/init.d/${NAME} start

# EOF

